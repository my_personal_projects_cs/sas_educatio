/*Set up libraries with necessary data*/
%let path=/Data1/IPEDSData/DataFiles/GraduationRates-GR;
%let libraryName=IPEDGR;
libname &libraryName "&path/SASData";
%let path=/Data1/IPEDSData/DataFiles/Salaries-SAL;
%let libraryName=ipedsal;
libname &libraryName "&path/SASData";
%let path=/Data1/IPEDSData/DataFiles/InstCharacteristics-IC;
%let libraryName=ipedicm;
libname &libraryName "&path/SASData";
%let path=/Data1/IPEDSData/DataFiles/FinancialAid-SFA;
%let libraryName=ipedsfa;
libname &libraryName "&path/SASData";
/*Get male and female percentages for incoming and graduates*/
data dist;
    set ipedgr.gr2021;
    by unitid; 
    keep unitid grtype grtotlt grtotlm grtotlw totm totw;
    where grtype in (6,9) and grtotlm > 0 and grtotlw > 0;
    totm = grtotlm/grtotlt *100;
    totw = grtotlw/grtotlt *100;
run;
/*Transpose to get incoming and graduate percentages as columns for both male and female */
proc transpose data=dist out=trans(rename=(col1=in col2=out));
	by unitid;
  	var totm totw;
run;

proc sql;
create table without as
select * from trans where in > 5 and out ne . ;
quit;
/*Subtract graduating from incoming to get the change in distribution (I kept change in men)*/
data responsevar;
	set without;
	keep unitid deltam;
	by unitid;
	deltam = out - in;
	where _NAME_ EQ 'totm';
	if deltam ne .;
run;

/* adding wieght*/
proc sql;
 create table responsevarweight as
 select a.*, b.grtotlt ,LOG10(b.grtotlt) as grtotltlog from responsevar a left join(
 select unitid, grtotlt from dist where  grtype eq 6) b on a.unitid eq b.unitid;
 quit;

%let path=/Data1/IPEDSData/DataFiles/Salaries-SAL;
%let libraryName=ipedsal;
libname &libraryName "&path/SASData";
%let path=/Data1/IPEDSData/DataFiles/InstCharacteristics-IC;
%let libraryName=ipedicm;
libname &libraryName "&path/SASData";
%let path=/Data1/IPEDSData/DataFiles/FinancialAid-SFA;
%let libraryName=ipedsfa;
libname &libraryName "&path/SASData";

/*** Average Salary across instructional staff ***/
data salarypredictor;
    set ipedsal.sal2021_is;
    keep unitid avgsal avgsalW avgsalM;
    where arank eq 7;
    avgsal = saoutlt/sainstt;
    avgsalW = saoutlw/sainstw;
    avgsalM = saoutlm/sainstw;
run;
/**Creating in and out of state cost of attendence predictors**/
data costattenpredictor;
	set ipedicm.ic2021_ay;
	keep unitid cost_in cost_out;
	avg_cost = SUM(chg5ay2, chg6ay2, chg7ay2, chg8ay2)/2;
	cost_in = chg2ay2 + chg4ay2 + avg_cost;
	cost_out = chg3ay2 + chg4ay2 + avg_cost;
	where chg2ay2 ne . and chg3ay2 ne . and chg4ay2 ne .;
run;
/**Average percent of cost of attendence covered by financial aid**/
data pctcovfinaidpredictor;
	merge ipedsfa.sfa2021 costattenpredictor;
	by unitid;
	if uagrnta ne . and cost_in ne . and cost_out ne .;
	keep unitid cost_in cost_out pctcost;
	if scfa13p = . then do;
		per_stu = cost_out;
		end;
	else do;
		per_stu = ((cost_out) * scfa13p/100) + ((cost_in) * (1-(scfa13p/100)));
		end;
	pctcost = 100 * (uagrnta / per_stu);
run;





proc format;
value obereg  
0='U.S. Service schools' 
1,2='New England and Mid East' 
3,4='Great Lakes and Plains' 
5='Southeast (AL, AR, FL, GA, KY, LA, MS, NC, SC, TN, VA, WV)' 
6='Southwest (AZ, NM, OK, TX)' 
7='Rocky Mountains (CO, ID, MT, UT, WY)' 
8='Far West (AK, CA, HI, NV, OR, WA)' 
9='Other U.S. jurisdictions (AS, FM, GU, MH, MP, PR, PW, VI)'
;
run;

data merges;
merge responsevarweight(in=a) ipedicm.ic2021_ay(in=b)  ipedsfa.sfa2021(in=d) ipedicm.hd2021(in=e) ipedicm.ic2021(in=f);
by unitid;
if a and b and d and e and f;
drop x:;
run;

data preds;
 set merges;
 where not missing(tuition2) and uagrnta ne . and upgrnta ne . and ufloana ne .;
 AvgGrant = uagrnta;
 AvgPell = upgrnta;
 AvgFed = ufloana;
 obereg2 = put(obereg,obereg.);
format instsize instsize.;  
 keep unitid AvgGrant AvgPell AvgFed obereg obereg2;
run;


proc sql;
create table firstjoin as
SELECT salarypredictor.unitid, responsevarweight.deltam, grtotlt,grtotltlog, avgsal, avgsalW, avgsalM
FROM salarypredictor
INNER JOIN responsevarweight
ON salarypredictor.unitid = responsevarweight.unitid
where avgsal ne .;
run;

proc sql;
create table secondjoin as
SELECT pctcovfinaidpredictor.unitid, firstjoin.deltam, grtotlt,grtotltlog, avgsal,avgsalW, avgsalM, cost_in, cost_out, pctcost
FROM pctcovfinaidpredictor
INNER JOIN firstjoin
ON pctcovfinaidpredictor.unitid = firstjoin.unitid;
run;

proc sql; 
create table finjoin as
SELECT distinct *
FROM secondjoin 
INNER JOIN preds
ON secondjoin.unitid = preds.unitid;
run;




proc standard data=finjoin mean=0 std=1
    out=standardized_data;
    var   avgsal avgsalW avgsalM cost_out cost_in pctcost AvgGrant AvgPell AvgFed ; /* List the variables you want to standardize */
run;


proc glmselect data=standardized_data;
where obereg2 ^= 'U.S. Service schools';
	class  obereg2;
	model deltam = avgsal|avgsalW|avgsalM|AvgGrant|AvgPell|AvgFed|pctcost|obereg2@2/ hierarchy=single selection=stepwise(slentry=0.05 slstay=0.05 select=sl stop=sl choose=adjrsq);
	weight grtotlt;
run;



