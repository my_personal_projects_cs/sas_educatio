%let path=/Data1/IPEDSData/DataFiles/OutcomeMeas-OM;
%let file=om2021;
%let libraryName=IPEDADM;
libname &libraryName "&path/SASData";

Proc Format library=&libraryName;
	value omchrt 50='Total entering, Total' 
		51='Total entering, Pell Grant recipients' 
		52='Total entering, Non-Pell Grant recipients' 
		10='First-time, full-time entering, Total' 
		11='First-time, full-time entering, Pell Grant recipients' 
		12='First-time, full-time entering, Non-Pell Grant recipients' 
		20='First-time, part-time entering, Total' 
		21='First-time, part-time entering, Pell Grant recipients' 
		22='First-time, part-time entering, Non-Pell Grant recipients' 
		30='Non-first-time, full-time entering, Total' 
		31='Non-first-time, full-time entering, Pell Grant recipients' 
		32='Non-first-time, full-time entering, Non-Pell Grant recipients' 
		40='Non-first-time, part-time entering, Total' 
		41='Non-first-time, part-time entering, Pell Grant recipients' 
		42='Non-first-time, part-time entering, Non-Pell Grant recipients';
	value $ximpflg A='Not applicable' B='Institution left item blank' 
		C='Analyst corrected reported value' D='Do not know' 
		G='Data generated from other data values' 
		H='Value not derived - data not usable' J='Logical imputation' 
		K='Ratio adjustment' L='Imputed using the Group Median procedure' 
		N='Imputed using Nearest Neighbor procedure' 
		P='Imputed using Carry Forward procedure' R='Reported' 
		Y='Specific professional practice program n' Z='Implied zero';
run;

options fmtsearch=(&libraryName);

Data &libraryName..&file;;
	infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 
		lrecl=32736;
	informat unitid 6. omchrt 2. xomrchrt $1. omrchrt 6. xomexcls $1. omexcls 6. 
		xomachrt $1. omachrt 6. xomcert4 $1. omcert4 6. xomassc4 $1. omassc4 6. 
		xombach4 $1. ombach4 6. xomawdn4 $1. omawdn4 6. xomawdp4 $1. omawdp4 3. 
		xomcert6 $1. omcert6 6. xomassc6 $1. omassc6 6. xombach6 $1. ombach6 6. 
		xomawdn6 $1. omawdn6 6. xomawdp6 $1. omawdp6 3. xomcert8 $1. omcert8 6. 
		xomassc8 $1. omassc8 6. xombach8 $1. ombach8 6. xomawdn8 $1. omawdn8 6. 
		xomenryi $1. omenryi 6. xomenrai $1. omenrai 6. xomenrun $1. omenrun 6. 
		xomnoawd $1. omnoawd 6. xomawdp8 $1. omawdp8 3. xomenrtp $1. omenrtp 3. 
		xomenryp $1. omenryp 3. xomenrap $1. omenrap 3. xomenrup $1. omenrup 3.;
	input unitid omchrt xomrchrt $
omrchrt xomexcls $
omexcls xomachrt $
omachrt xomcert4 $
omcert4 xomassc4 $
omassc4 xombach4 $
ombach4 xomawdn4 $
omawdn4 xomawdp4 $
omawdp4 xomcert6 $
omcert6 xomassc6 $
omassc6 xombach6 $
ombach6 xomawdn6 $
omawdn6 xomawdp6 $
omawdp6 xomcert8 $
omcert8 xomassc8 $
omassc8 xombach8 $
ombach8 xomawdn8 $
omawdn8 xomenryi $
omenryi xomenrai $
omenrai xomenrun $
omenrun xomnoawd $
omnoawd xomawdp8 $
omawdp8 xomenrtp $
omenrtp xomenryp $
omenryp xomenrap $
omenrap xomenrup $
omenrup;
	label unitid='Unique identification number of the institution' 
		omchrt='Cohort category' 
		xomrchrt='Imputation field for omrchrt - 2013-14 cohort' 
		omrchrt='2013-14 cohort' 
		xomexcls='Imputation field for omexcls - Exclusions to 2013-14 cohort' 
		omexcls='Exclusions to 2013-14 cohort' 
		xomachrt='Imputation field for omachrt - Adjusted 2013-14 cohort' 
		omachrt='Adjusted 2013-14 cohort' xomcert4='Imputation field for omcert4 - Number of adjusted cohort receiving a certificate at 4 years (August 31, 2017)' omcert4='Number of adjusted cohort receiving a certificate at 4 years (August 31, 2017)' xomassc4='Imputation field for omassc4 - Number of adjusted cohort receiving an Associate^s degree at 4 years (August 31, 2017)' omassc4='Number of adjusted cohort receiving an Associate^s degree at 4 years (August 31, 2017)' xombach4='Imputation field for ombach4 - Number of adjusted cohort receiving a Bachelor^s degree at 4 years (August 31, 2017)' ombach4='Number of adjusted cohort receiving a Bachelor^s degree at 4 years (August 31, 2017)' xomawdn4='Imputation field for omawdn4 - Number of adjusted cohort receiving an award at 4 years (August 31, 2017)' omawdn4='Number of adjusted cohort receiving an award at 4 years (August 31, 2017)' xomawdp4='Imputation field for omawdp4 - Percent of adjusted cohort receiving an award at 4 years (August 31, 2017)' omawdp4='Percent of adjusted cohort receiving an award at 4 years (August 31, 2017)' xomcert6='Imputation field for omcert6 - Number of adjusted cohort receiving a certificate at 6 years (August 31, 2019)' omcert6='Number of adjusted cohort receiving a certificate at 6 years (August 31, 2019)' xomassc6='Imputation field for omassc6 - Number of adjusted cohort receiving an Associate^s degree at 6 years (August 31, 2019)' omassc6='Number of adjusted cohort receiving an Associate^s degree at 6 years (August 31, 2019)' xombach6='Imputation field for ombach6 - Number of adjusted cohort receiving a Bachelor^s degree at 6 years (August 31, 2019)' ombach6='Number of adjusted cohort receiving a Bachelor^s degree at 6 years (August 31, 2019)' xomawdn6='Imputation field for omawdn6 - Number of adjusted cohort receiving an award at 6 years (August 31, 2019)' omawdn6='Number of adjusted cohort receiving an award at 6 years (August 31, 2019)' xomawdp6='Imputation field for omawdp6 - Percent of adjusted cohort receiving an award at 6 years (August 31, 2019)' omawdp6='Percent of adjusted cohort receiving an award at 6 years (August 31, 2019)' xomcert8='Imputation field for omcert8 - Number of adjusted cohort receiving a certificate at 8 years (August 31, 2021)' omcert8='Number of adjusted cohort receiving a certificate at 8 years (August 31, 2021)' xomassc8='Imputation field for omassc8 - Number of adjusted cohort receiving an Associate^s degree at 8 years (August 31, 2021)' omassc8='Number of adjusted cohort receiving an Associate^s degree at 8 years (August 31, 2021)' xombach8='Imputation field for ombach8 - Number of adjusted cohort receiving a Bachelor^s degree at 8 years (August 31, 2021)' ombach8='Number of adjusted cohort receiving a Bachelor^s degree at 8 years (August 31, 2021)' xomawdn8='Imputation field for omawdn8 - Number of adjusted cohort receiving an award at 8 years (August 31, 2021)' omawdn8='Number of adjusted cohort receiving an award at 8 years (August 31, 2021)' xomenryi='Imputation field for omenryi - Number of adjusted cohort still enrolled at your institution at 8 years (August 31, 2021)' omenryi='Number of adjusted cohort still enrolled at your institution at 8 years (August 31, 2021)' xomenrai='Imputation field for omenrai - Number of adjusted cohort who enrolled subsequently at another institution at 8 years (August 31, 2021)' omenrai='Number of adjusted cohort who enrolled subsequently at another institution at 8 years (August 31, 2021)' xomenrun='Imputation field for omenrun - Number of adjusted cohort whose subsequent enrollment status is unknown at 8 years (August 31, 2021)' omenrun='Number of adjusted cohort whose subsequent enrollment status is unknown at 8 years (August 31, 2021)' xomnoawd='Imputation field for omnoawd - Number of adjusted cohort who did not receive an award from your institution at 8 years (August 31, 2021)' omnoawd='Number of adjusted cohort who did not receive an award from your institution at 8 years (August 31, 2021)' xomawdp8='Imputation field for omawdp8 - Percent of adjusted cohort receiving an award at 8 years (August 31, 2021)' omawdp8='Percent of adjusted cohort receiving an award at 8 years (August 31, 2021)' xomenrtp='Imputation field for omenrtp - Percent of adjusted cohort still or subsequently enrolled at 8 years (August 31, 2021)' omenrtp='Percent of adjusted cohort still or subsequently enrolled at 8 years (August 31, 2021)' xomenryp='Imputation field for omenryp - Percent of adjusted cohort still enrolled at your institution at 8 years (August 31, 2021)' omenryp='Percent of adjusted cohort still enrolled at your institution at 8 years (August 31, 2021)' xomenrap='Imputation field for omenrap - Percent of adjusted cohort  enrolled subsequently at another institution at 8 years (August 31, 2021)' omenrap='Percent of adjusted cohort  enrolled subsequently at another institution at 8 years (August 31, 2021)' xomenrup='Imputation field for omenrup - Percent of adjusted cohort  enrollment status unknown at 8 years (August 31, 2021)' omenrup='Percent of adjusted cohort  enrollment status unknown at 8 years (August 31, 2021)';
	format xomrchrt-character-xomenrup $ximpflg. omchrt omchrt.;
run;


Proc Freq;
	Tables omchrt xomrchrt xomexcls xomachrt xomcert4 xomassc4 xombach4 xomawdn4 
		xomawdp4 xomcert6 xomassc6 xombach6 xomawdn6 xomawdp6 xomcert8 xomassc8 
		xombach8 xomawdn8 xomenryi xomenrai xomenrun xomnoawd xomawdp8 xomenrtp 
		xomenryp xomenrap xomenrup / missing;

Proc Summary print n sum mean min max;
	var omrchrt omexcls omachrt omcert4 omassc4 ombach4 omawdn4 omawdp4 omcert6 
		omassc6 ombach6 omawdn6 omawdp6 omcert8 omassc8 ombach8 omawdn8 omenryi 
		omenrai omenrun omnoawd omawdp8 omenrtp omenryp omenrap omenrup;
run;