*** Created: September 15, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\f2021_f1a.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
xf1a01   $1.
f1a01    12. 
xf1a31   $1.
f1a31    12. 
xf1a04   $1.
f1a04    12. 
xf1a05   $1.
f1a05    12. 
xf1a06   $1.
f1a06    12. 
xf1a19   $1.
f1a19    12. 
xf1a07   $1.
f1a07    12. 
xf1a08   $1.
f1a08    12. 
xf1a09   $1.
f1a09    12. 
xf1a10   $1.
f1a10    12. 
xf1a11   $1.
f1a11    12. 
xf1a12   $1.
f1a12    12. 
xf1a13   $1.
f1a13    12. 
xf1a20   $1.
f1a20    12. 
xf1a14   $1.
f1a14    12. 
xf1a15   $1.
f1a15    12. 
xf1a16   $1.
f1a16    12. 
xf1a17   $1.
f1a17    12. 
xf1a18   $1.
f1a18    12. 
xf1a214  $1.
f1a214   12. 
xf1a224  $1.
f1a224   12. 
xf1a234  $1.
f1a234   12. 
xf1a324  $1.
f1a324   12. 
xf1a274  $1.
f1a274   12. 
xf1a27t4 $1.
f1a27t4  12. 
xf1a284  $1.
f1a284   12. 
xf1a334  $1.
f1a334   12. 
xf1a344  $1.
f1a344   12. 
xf1d01   $1.
f1d01    12. 
xf1d02   $1.
f1d02    12. 
xf1d03   $1.
f1d03    12. 
xf1d04   $1.
f1d04    12. 
xf1d05   $1.
f1d05    12. 
xf1d06   $1.
f1d06    12. 
xf1b01   $1.
f1b01    12. 
xf1b02   $1.
f1b02    12. 
xf1b03   $1.
f1b03    12. 
xf1b04   $1.
f1b04    12. 
xf1b04a  $1.
f1b04a   12. 
xf1b04b  $1.
f1b04b   12. 
xf1b05   $1.
f1b05    12. 
xf1b06   $1.
f1b06    12. 
xf1b26   $1.
f1b26    12. 
xf1b07   $1.
f1b07    12. 
xf1b08   $1.
f1b08    12. 
xf1b09   $1.
f1b09    12. 
xf1b10   $1.
f1b10    12. 
xf1b11   $1.
f1b11    12. 
xf1b12   $1.
f1b12    12. 
xf1b13   $1.
f1b13    12. 
xf1b14   $1.
f1b14    12. 
xf1b15   $1.
f1b15    12. 
xf1b16   $1.
f1b16    12. 
xf1b17   $1.
f1b17    12. 
xf1b18   $1.
f1b18    12. 
xf1b19   $1.
f1b19    12. 
xf1b27   $1.
f1b27    12. 
xf1b20   $1.
f1b20    12. 
xf1b21   $1.
f1b21    12. 
xf1b22   $1.
f1b22    12. 
xf1b23   $1.
f1b23    12. 
xf1b24   $1.
f1b24    12. 
xf1b25   $1.
f1b25    12. 
xf1c011  $1.
f1c011   12. 
xf1c012  $1.
f1c012   12. 
xf1c021  $1.
f1c021   12. 
xf1c022  $1.
f1c022   12. 
xf1c031  $1.
f1c031   12. 
xf1c032  $1.
f1c032   12. 
xf1c051  $1.
f1c051   12. 
xf1c052  $1.
f1c052   12. 
xf1c061  $1.
f1c061   12. 
xf1c062  $1.
f1c062   12. 
xf1c071  $1.
f1c071   12. 
xf1c072  $1.
f1c072   12. 
xf1c101  $1.
f1c101   12. 
xf1c111  $1.
f1c111   12. 
xf1c112  $1.
f1c112   12. 
xf1c121  $1.
f1c121   12. 
xf1c122  $1.
f1c122   12. 
xf1c131  $1.
f1c131   12. 
xf1c132  $1.
f1c132   12. 
xf1c141  $1.
f1c141   12. 
xf1c142  $1.
f1c142   12. 
xf1c191  $1.
f1c191   12. 
xf1c192  $1.
f1c192   12. 
xf1c193  $1.
f1c193   12. 
xf1c19om $1.
f1c19om  12. 
xf1c19dp $1.
f1c19dp  12. 
xf1c19in $1.
f1c19in  12. 
xf1c19ot $1.
f1c19ot  12. 
f1mhp    2. 
xf1m01   $1.
f1m01    12. 
xf1m02   $1.
f1m02    12. 
xf1m03   $1.
f1m03    12. 
xf1m04   $1.
f1m04    12. 
f1mhop   2. 
xf1m05   $1.
f1m05    12. 
xf1m06   $1.
f1m06    12. 
xf1m07   $1.
f1m07    12. 
xf1m08   $1.
f1m08    12. 
xf1e01   $1.
f1e01    12. 
xf1e02   $1.
f1e02    12. 
xf1e03   $1.
f1e03    12. 
xf1e04   $1.
f1e04    12. 
xf1e05   $1.
f1e05    12. 
xf1e06   $1.
f1e06    12. 
xf1e07   $1.
f1e07    12. 
xf1e08   $1.
f1e08    12. 
xf1e09   $1.
f1e09    12. 
xf1e10   $1.
f1e10    12. 
xf1e11   $1.
f1e11    12. 
xf1e12   $1.
f1e12    12. 
xf1e121  $1.
f1e121   12. 
xf1e122  $1.
f1e122   12. 
xf1e13   $1.
f1e13    12. 
xf1e131  $1.
f1e131   12. 
xf1e132  $1.
f1e132   12. 
xf1e14   $1.
f1e14    12. 
xf1e141  $1.
f1e141   12. 
xf1e142  $1.
f1e142   12. 
xf1e15   $1.
f1e15    12. 
xf1e151  $1.
f1e151   12. 
xf1e152  $1.
f1e152   12. 
xf1e16   $1.
f1e16    12. 
xf1e161  $1.
f1e161   12. 
xf1e162  $1.
f1e162   12. 
xf1e17   $1.
f1e17    12. 
xf1e171  $1.
f1e171   12. 
xf1e172  $1.
f1e172   12. 
f1fha    2. 
xf1h01   $1.
f1h01    12. 
xf1h02   $1.
f1h02    12. 
xf1h03   $1.
f1h03    12. 
xf1h03a  $1.
f1h03a   12. 
xf1h03b  $1.
f1h03b   12. 
xf1h03c  $1.
f1h03c   12. 
xf1h03d  $1.
f1h03d   12. 
xf1n01   $1.
f1n01    12. 
xf1n02   $1.
f1n02    12. 
xf1n03   $1.
f1n03    12. 
xf1n04   $1.
f1n04    12. 
xf1n05   $1.
f1n05    12. 
xf1n06   $1.
f1n06    12. 
xf1n07   $1.
f1n07    12.;

input
unitid   
xf1a01   $
f1a01    
xf1a31   $
f1a31    
xf1a04   $
f1a04    
xf1a05   $
f1a05    
xf1a06   $
f1a06    
xf1a19   $
f1a19    
xf1a07   $
f1a07    
xf1a08   $
f1a08    
xf1a09   $
f1a09    
xf1a10   $
f1a10    
xf1a11   $
f1a11    
xf1a12   $
f1a12    
xf1a13   $
f1a13    
xf1a20   $
f1a20    
xf1a14   $
f1a14    
xf1a15   $
f1a15    
xf1a16   $
f1a16    
xf1a17   $
f1a17    
xf1a18   $
f1a18    
xf1a214  $
f1a214   
xf1a224  $
f1a224   
xf1a234  $
f1a234   
xf1a324  $
f1a324   
xf1a274  $
f1a274   
xf1a27t4 $
f1a27t4  
xf1a284  $
f1a284   
xf1a334  $
f1a334   
xf1a344  $
f1a344   
xf1d01   $
f1d01    
xf1d02   $
f1d02    
xf1d03   $
f1d03    
xf1d04   $
f1d04    
xf1d05   $
f1d05    
xf1d06   $
f1d06    
xf1b01   $
f1b01    
xf1b02   $
f1b02    
xf1b03   $
f1b03    
xf1b04   $
f1b04    
xf1b04a  $
f1b04a   
xf1b04b  $
f1b04b   
xf1b05   $
f1b05    
xf1b06   $
f1b06    
xf1b26   $
f1b26    
xf1b07   $
f1b07    
xf1b08   $
f1b08    
xf1b09   $
f1b09    
xf1b10   $
f1b10    
xf1b11   $
f1b11    
xf1b12   $
f1b12    
xf1b13   $
f1b13    
xf1b14   $
f1b14    
xf1b15   $
f1b15    
xf1b16   $
f1b16    
xf1b17   $
f1b17    
xf1b18   $
f1b18    
xf1b19   $
f1b19    
xf1b27   $
f1b27    
xf1b20   $
f1b20    
xf1b21   $
f1b21    
xf1b22   $
f1b22    
xf1b23   $
f1b23    
xf1b24   $
f1b24    
xf1b25   $
f1b25    
xf1c011  $
f1c011   
xf1c012  $
f1c012   
xf1c021  $
f1c021   
xf1c022  $
f1c022   
xf1c031  $
f1c031   
xf1c032  $
f1c032   
xf1c051  $
f1c051   
xf1c052  $
f1c052   
xf1c061  $
f1c061   
xf1c062  $
f1c062   
xf1c071  $
f1c071   
xf1c072  $
f1c072   
xf1c101  $
f1c101   
xf1c111  $
f1c111   
xf1c112  $
f1c112   
xf1c121  $
f1c121   
xf1c122  $
f1c122   
xf1c131  $
f1c131   
xf1c132  $
f1c132   
xf1c141  $
f1c141   
xf1c142  $
f1c142   
xf1c191  $
f1c191   
xf1c192  $
f1c192   
xf1c193  $
f1c193   
xf1c19om $
f1c19om  
xf1c19dp $
f1c19dp  
xf1c19in $
f1c19in  
xf1c19ot $
f1c19ot  
f1mhp    
xf1m01   $
f1m01    
xf1m02   $
f1m02    
xf1m03   $
f1m03    
xf1m04   $
f1m04    
f1mhop   
xf1m05   $
f1m05    
xf1m06   $
f1m06    
xf1m07   $
f1m07    
xf1m08   $
f1m08    
xf1e01   $
f1e01    
xf1e02   $
f1e02    
xf1e03   $
f1e03    
xf1e04   $
f1e04    
xf1e05   $
f1e05    
xf1e06   $
f1e06    
xf1e07   $
f1e07    
xf1e08   $
f1e08    
xf1e09   $
f1e09    
xf1e10   $
f1e10    
xf1e11   $
f1e11    
xf1e12   $
f1e12    
xf1e121  $
f1e121   
xf1e122  $
f1e122   
xf1e13   $
f1e13    
xf1e131  $
f1e131   
xf1e132  $
f1e132   
xf1e14   $
f1e14    
xf1e141  $
f1e141   
xf1e142  $
f1e142   
xf1e15   $
f1e15    
xf1e151  $
f1e151   
xf1e152  $
f1e152   
xf1e16   $
f1e16    
xf1e161  $
f1e161   
xf1e162  $
f1e162   
xf1e17   $
f1e17    
xf1e171  $
f1e171   
xf1e172  $
f1e172   
f1fha    
xf1h01   $
f1h01    
xf1h02   $
f1h02    
xf1h03   $
f1h03    
xf1h03a  $
f1h03a   
xf1h03b  $
f1h03b   
xf1h03c  $
f1h03c   
xf1h03d  $
f1h03d   
xf1n01   $
f1n01    
xf1n02   $
f1n02    
xf1n03   $
f1n03    
xf1n04   $
f1n04    
xf1n05   $
f1n05    
xf1n06   $
f1n06    
xf1n07   $
f1n07   ;

label
unitid  ='Unique identification number of the institution' 
xf1a01  ='Imputation field for f1a01 - Total current assets'
f1a01   ='Total current assets' 
xf1a31  ='Imputation field for f1a31 - Depreciable capital assets, net of depreciation'
f1a31   ='Depreciable capital assets, net of depreciation' 
xf1a04  ='Imputation field for f1a04 - Other noncurrent assets'
f1a04   ='Other noncurrent assets' 
xf1a05  ='Imputation field for f1a05 - Total noncurrent assets'
f1a05   ='Total noncurrent assets' 
xf1a06  ='Imputation field for f1a06 - Total assets'
f1a06   ='Total assets' 
xf1a19  ='Imputation field for f1a19 - Deferred outflows of resources'
f1a19   ='Deferred outflows of resources' 
xf1a07  ='Imputation field for f1a07 - Long-term debt, current portion'
f1a07   ='Long-term debt, current portion' 
xf1a08  ='Imputation field for f1a08 - Other current liabilities'
f1a08   ='Other current liabilities' 
xf1a09  ='Imputation field for f1a09 - Total current liabilities'
f1a09   ='Total current liabilities' 
xf1a10  ='Imputation field for f1a10 - Long-term debt'
f1a10   ='Long-term debt' 
xf1a11  ='Imputation field for f1a11 - Other noncurrent liabilities'
f1a11   ='Other noncurrent liabilities' 
xf1a12  ='Imputation field for f1a12 - Total noncurrent liabilities'
f1a12   ='Total noncurrent liabilities' 
xf1a13  ='Imputation field for f1a13 - Total liabilities'
f1a13   ='Total liabilities' 
xf1a20  ='Imputation field for f1a20 - Deferred inflows of resources'
f1a20   ='Deferred inflows of resources' 
xf1a14  ='Imputation field for f1a14 - Invested in capital assets, net of related debt'
f1a14   ='Invested in capital assets, net of related debt' 
xf1a15  ='Imputation field for f1a15 - Restricted-expendable'
f1a15   ='Restricted-expendable' 
xf1a16  ='Imputation field for f1a16 - Restricted-nonexpendable'
f1a16   ='Restricted-nonexpendable' 
xf1a17  ='Imputation field for f1a17 - Unrestricted'
f1a17   ='Unrestricted' 
xf1a18  ='Imputation field for f1a18 - Net position'
f1a18   ='Net position' 
xf1a214 ='Imputation field for f1a214 - Land  improvements - Ending balance'
f1a214  ='Land  improvements - Ending balance' 
xf1a224 ='Imputation field for f1a224 - Infrastructure - Ending balance'
f1a224  ='Infrastructure - Ending balance' 
xf1a234 ='Imputation field for f1a234 - Buildings - Ending balance'
f1a234  ='Buildings - Ending balance' 
xf1a324 ='Imputation field for f1a324 - Equipment, including art and library collections - Ending balance'
f1a324  ='Equipment, including art and library collections - Ending balance' 
xf1a274 ='Imputation field for f1a274 - Construction in progress - Ending balance'
f1a274  ='Construction in progress - Ending balance' 
xf1a27t4='Imputation field for f1a27t4 - Total for plant, property and equipment - Ending balance'
f1a27t4 ='Total for plant, property and equipment - Ending balance' 
xf1a284 ='Imputation field for f1a284 - Accumulated depreciation - Ending balance'
f1a284  ='Accumulated depreciation - Ending balance' 
xf1a334 ='Imputation field for f1a334 - Intangible assets , net of accumulated amortization - Ending balance'
f1a334  ='Intangible assets , net of accumulated amortization - Ending balance' 
xf1a344 ='Imputation field for f1a344 - Other capital assets - Ending balance (New Aligned)'
f1a344  ='Other capital assets - Ending balance (New Aligned)' 
xf1d01  ='Imputation field for f1d01 - Total revenues and other additions'
f1d01   ='Total revenues and other additions' 
xf1d02  ='Imputation field for f1d02 - Total expenses and other deductions'
f1d02   ='Total expenses and other deductions' 
xf1d03  ='Imputation field for f1d03 - Change in net position during the year'
f1d03   ='Change in net position during the year' 
xf1d04  ='Imputation field for f1d04 - Net position beginning of year'
f1d04   ='Net position beginning of year' 
xf1d05  ='Imputation field for f1d05 - Adjustments to beginning net position'
f1d05   ='Adjustments to beginning net position' 
xf1d06  ='Imputation field for f1d06 - Net position end of year'
f1d06   ='Net position end of year' 
xf1b01  ='Imputation field for f1b01 - Tuition and fees, after deducting discounts and allowances'
f1b01   ='Tuition and fees, after deducting discounts and allowances' 
xf1b02  ='Imputation field for f1b02 - Federal operating grants and contracts'
f1b02   ='Federal operating grants and contracts' 
xf1b03  ='Imputation field for f1b03 - State operating grants and contracts'
f1b03   ='State operating grants and contracts' 
xf1b04  ='Imputation field for f1b04 - Local/private operating grants and contracts'
f1b04   ='Local/private operating grants and contracts' 
xf1b04a ='Imputation field for f1b04a - Local operating grants and contracts'
f1b04a  ='Local operating grants and contracts' 
xf1b04b ='Imputation field for f1b04b - Private operating grants and contracts'
f1b04b  ='Private operating grants and contracts' 
xf1b05  ='Imputation field for f1b05 - Sales and services of auxiliary enterprises'
f1b05   ='Sales and services of auxiliary enterprises' 
xf1b06  ='Imputation field for f1b06 - Sales and services of hospitals'
f1b06   ='Sales and services of hospitals' 
xf1b26  ='Imputation field for f1b26 - Sales and services of educational activities'
f1b26   ='Sales and services of educational activities' 
xf1b07  ='Imputation field for f1b07 - Independent operations'
f1b07   ='Independent operations' 
xf1b08  ='Imputation field for f1b08 - Other sources - operating'
f1b08   ='Other sources - operating' 
xf1b09  ='Imputation field for f1b09 - Total operating revenues'
f1b09   ='Total operating revenues' 
xf1b10  ='Imputation field for f1b10 - Federal appropriations'
f1b10   ='Federal appropriations' 
xf1b11  ='Imputation field for f1b11 - State appropriations'
f1b11   ='State appropriations' 
xf1b12  ='Imputation field for f1b12 - Local appropriations, education district taxes, and similar support'
f1b12   ='Local appropriations, education district taxes, and similar support' 
xf1b13  ='Imputation field for f1b13 - Federal nonoperating grants'
f1b13   ='Federal nonoperating grants' 
xf1b14  ='Imputation field for f1b14 - State nonoperating grants'
f1b14   ='State nonoperating grants' 
xf1b15  ='Imputation field for f1b15 - Local nonoperating grants'
f1b15   ='Local nonoperating grants' 
xf1b16  ='Imputation field for f1b16 - Gifts, including contributions from affiliated organizations'
f1b16   ='Gifts, including contributions from affiliated organizations' 
xf1b17  ='Imputation field for f1b17 - Investment income'
f1b17   ='Investment income' 
xf1b18  ='Imputation field for f1b18 - Other nonoperating revenues'
f1b18   ='Other nonoperating revenues' 
xf1b19  ='Imputation field for f1b19 - Total nonoperating revenues'
f1b19   ='Total nonoperating revenues' 
xf1b27  ='Imputation field for f1b27 - Total operating and nonoperating revenues'
f1b27   ='Total operating and nonoperating revenues' 
xf1b20  ='Imputation field for f1b20 - Capital appropriations'
f1b20   ='Capital appropriations' 
xf1b21  ='Imputation field for f1b21 - Capital grants and gifts'
f1b21   ='Capital grants and gifts' 
xf1b22  ='Imputation field for f1b22 - Additions to permanent endowments'
f1b22   ='Additions to permanent endowments' 
xf1b23  ='Imputation field for f1b23 - Other revenues and additions'
f1b23   ='Other revenues and additions' 
xf1b24  ='Imputation field for f1b24 - Total other revenues and additions'
f1b24   ='Total other revenues and additions' 
xf1b25  ='Imputation field for f1b25 - Total all revenues and other additions'
f1b25   ='Total all revenues and other additions' 
xf1c011 ='Imputation field for f1c011 - Instruction - Current year total'
f1c011  ='Instruction - Current year total' 
xf1c012 ='Imputation field for f1c012 - Instruction - Salaries and wages'
f1c012  ='Instruction - Salaries and wages' 
xf1c021 ='Imputation field for f1c021 - Research - Current year total'
f1c021  ='Research - Current year total' 
xf1c022 ='Imputation field for f1c022 - Research - Salaries and wages'
f1c022  ='Research - Salaries and wages' 
xf1c031 ='Imputation field for f1c031 - Public service - Current year total'
f1c031  ='Public service - Current year total' 
xf1c032 ='Imputation field for f1c032 - Public service - Salaries and wages'
f1c032  ='Public service - Salaries and wages' 
xf1c051 ='Imputation field for f1c051 - Academic support - Current year total'
f1c051  ='Academic support - Current year total' 
xf1c052 ='Imputation field for f1c052 - Academic support - Salaries and wages'
f1c052  ='Academic support - Salaries and wages' 
xf1c061 ='Imputation field for f1c061 - Student services - Current year total'
f1c061  ='Student services - Current year total' 
xf1c062 ='Imputation field for f1c062 - Student services - Salaries and wages'
f1c062  ='Student services - Salaries and wages' 
xf1c071 ='Imputation field for f1c071 - Institutional support - Current year total'
f1c071  ='Institutional support - Current year total' 
xf1c072 ='Imputation field for f1c072 - Institutional support - Salaries and wages'
f1c072  ='Institutional support - Salaries and wages' 
xf1c101 ='Imputation field for f1c101 - Scholarships and fellowships expenses -- Current year total'
f1c101  ='Scholarships and fellowships expenses -- Current year total' 
xf1c111 ='Imputation field for f1c111 - Auxiliary enterprises -- Current year total'
f1c111  ='Auxiliary enterprises -- Current year total' 
xf1c112 ='Imputation field for f1c112 - Auxiliary enterprises -- Salaries and wages'
f1c112  ='Auxiliary enterprises -- Salaries and wages' 
xf1c121 ='Imputation field for f1c121 - Hospital services - Current year total'
f1c121  ='Hospital services - Current year total' 
xf1c122 ='Imputation field for f1c122 - Hospital services - Salaries and wages'
f1c122  ='Hospital services - Salaries and wages' 
xf1c131 ='Imputation field for f1c131 - Independent operations - Current year total'
f1c131  ='Independent operations - Current year total' 
xf1c132 ='Imputation field for f1c132 - Independent operations - Salaries and wages'
f1c132  ='Independent operations - Salaries and wages' 
xf1c141 ='Imputation field for f1c141 - Other expenses  deductions - Current year total'
f1c141  ='Other expenses  deductions - Current year total' 
xf1c142 ='Imputation field for f1c142 - Other expenses  deductions - Salaries and wages'
f1c142  ='Other expenses  deductions - Salaries and wages' 
xf1c191 ='Imputation field for f1c191 - Total expenses and deductions - Current year total'
f1c191  ='Total expenses and deductions - Current year total' 
xf1c192 ='Imputation field for f1c192 - Total expenses  expenses and deductions - Salaries and wages'
f1c192  ='Total expenses  expenses and deductions - Salaries and wages' 
xf1c193 ='Imputation field for f1c193 - Total expenses and deductions - Employee fringe benefits'
f1c193  ='Total expenses and deductions - Employee fringe benefits' 
xf1c19om='Imputation field for f1c19om - Total expenses and deductions - Operations and maintenance of plant'
f1c19om ='Total expenses and deductions - Operations and maintenance of plant' 
xf1c19dp='Imputation field for f1c19dp - Total expenses  and deductions - Depreciation'
f1c19dp ='Total expenses  and deductions - Depreciation' 
xf1c19in='Imputation field for f1c19in - Total expenses deductions - Interest'
f1c19in ='Total expenses deductions - Interest' 
xf1c19ot='Imputation field for f1c19ot - Total expenses and deductions - Other Natural Expenses and Deductions'
f1c19ot ='Total expenses and deductions - Other Natural Expenses and Deductions' 
f1mhp   ='Pension information reported' 
xf1m01  ='Imputation field for f1m01 - Pension expense'
f1m01   ='Pension expense' 
xf1m02  ='Imputation field for f1m02 - Net pension liability'
f1m02   ='Net pension liability' 
xf1m03  ='Imputation field for f1m03 - Deferred inflows of resources related to pension'
f1m03   ='Deferred inflows of resources related to pension' 
xf1m04  ='Imputation field for f1m04 - Deferred outflows of resources related to pension'
f1m04   ='Deferred outflows of resources related to pension' 
f1mhop  ='Postemployment benefits other than pension (OPEB)  reported' 
xf1m05  ='Imputation field for f1m05 - Other postemployment benefit (OPEB) expense'
f1m05   ='Other postemployment benefit (OPEB) expense' 
xf1m06  ='Imputation field for f1m06 - Other postemployment benefit (OPEB) net  liability'
f1m06   ='Other postemployment benefit (OPEB) net  liability' 
xf1m07  ='Imputation field for f1m07 - Deferred inflows related to other postemployment benefit (OPEB)'
f1m07   ='Deferred inflows related to other postemployment benefit (OPEB)' 
xf1m08  ='Imputation field for f1m08 - Deferred outflows related to other postemployment benefit (OPEB)'
f1m08   ='Deferred outflows related to other postemployment benefit (OPEB)' 
xf1e01  ='Imputation field for f1e01 - Pell grants (federal)'
f1e01   ='Pell grants (federal)' 
xf1e02  ='Imputation field for f1e02 - Other federal grants'
f1e02   ='Other federal grants' 
xf1e03  ='Imputation field for f1e03 - Grants by state government'
f1e03   ='Grants by state government' 
xf1e04  ='Imputation field for f1e04 - Grants by local government'
f1e04   ='Grants by local government' 
xf1e05  ='Imputation field for f1e05 - Institutional grants from restricted resources'
f1e05   ='Institutional grants from restricted resources' 
xf1e06  ='Imputation field for f1e06 - Institutional grants from unrestricted resources'
f1e06   ='Institutional grants from unrestricted resources' 
xf1e07  ='Imputation field for f1e07 - Total gross scholarships and fellowships'
f1e07   ='Total gross scholarships and fellowships' 
xf1e08  ='Imputation field for f1e08 - Discounts and allowances applied to tuition and fees'
f1e08   ='Discounts and allowances applied to tuition and fees' 
xf1e09  ='Imputation field for f1e09 - Discounts and allowances applied to sales & services of auxiliary enterprises'
f1e09   ='Discounts and allowances applied to sales & services of auxiliary enterprises' 
xf1e10  ='Imputation field for f1e10 - Total discounts and allowances'
f1e10   ='Total discounts and allowances' 
xf1e11  ='Imputation field for f1e11 - Net scholarships and fellowship expenses'
f1e11   ='Net scholarships and fellowship expenses' 
xf1e12  ='Imputation field for f1e12 - Total discounts and allowances from Pell grants'
f1e12   ='Total discounts and allowances from Pell grants' 
xf1e121 ='Imputation field for f1e121 - Discounts and allowances from Pell grants applied to tuition and fees'
f1e121  ='Discounts and allowances from Pell grants applied to tuition and fees' 
xf1e122 ='Imputation field for f1e122 - Discounts and allowances from Pell grants applied to auxiliary enterprises'
f1e122  ='Discounts and allowances from Pell grants applied to auxiliary enterprises' 
xf1e13  ='Imputation field for f1e13 - Total discounts and allowances from other federal grants'
f1e13   ='Total discounts and allowances from other federal grants' 
xf1e131 ='Imputation field for f1e131 - Discounts and allowances from other federal grants applied to tuition and fees'
f1e131  ='Discounts and allowances from other federal grants applied to tuition and fees' 
xf1e132 ='Imputation field for f1e132 - Discounts and allowances from other federal grants applied to auxiliary enterprises'
f1e132  ='Discounts and allowances from other federal grants applied to auxiliary enterprises' 
xf1e14  ='Imputation field for f1e14 - Total discounts and allowances from state government grants'
f1e14   ='Total discounts and allowances from state government grants' 
xf1e141 ='Imputation field for f1e141 - Discounts and allowances from state government grants applied to tuition and fees'
f1e141  ='Discounts and allowances from state government grants applied to tuition and fees' 
xf1e142 ='Imputation field for f1e142 - Discounts and allowances from state government grants applied to auxiliary enterprises'
f1e142  ='Discounts and allowances from state government grants applied to auxiliary enterprises' 
xf1e15  ='Imputation field for f1e15 - Discounts and allowances from local government grants'
f1e15   ='Discounts and allowances from local government grants' 
xf1e151 ='Imputation field for f1e151 - Discounts and allowances from local government grants applied to tuition and fees'
f1e151  ='Discounts and allowances from local government grants applied to tuition and fees' 
xf1e152 ='Imputation field for f1e152 - Discounts and allowances from local government grants applied to auxiliary
 enterprises'
f1e152  ='Discounts and allowances from local government grants applied to auxiliary
 enterprises' 
xf1e16  ='Imputation field for f1e16 - Total discounts and allowances from endowments and gifts'
f1e16   ='Total discounts and allowances from endowments and gifts' 
xf1e161 ='Imputation field for f1e161 - Discounts and allowances from endowments and gifts applied to tuition and fees'
f1e161  ='Discounts and allowances from endowments and gifts applied to tuition and fees' 
xf1e162 ='Imputation field for f1e162 - Discounts and allowances from endowments and gifts applied to auxiliary enterprises'
f1e162  ='Discounts and allowances from endowments and gifts applied to auxiliary enterprises' 
xf1e17  ='Imputation field for f1e17 - Total discounts and allowances from other institutional sources'
f1e17   ='Total discounts and allowances from other institutional sources' 
xf1e171 ='Imputation field for f1e171 - Discounts and
 allowances from other institutional sources applied to tuition and fees'
f1e171  ='Discounts and
 allowances from other institutional sources applied to tuition and fees' 
xf1e172 ='Imputation field for f1e172 - Discounts and allowances from other institutional sources applied to auxiliary enterprises'
f1e172  ='Discounts and allowances from other institutional sources applied to auxiliary enterprises' 
f1fha   ='Does this institution or any of its foundations or other affiliated organizations own endowment assets ?' 
xf1h01  ='Imputation field for f1h01 - Value of endowment assets at the beginning of the fiscal year'
f1h01   ='Value of endowment assets at the beginning of the fiscal year' 
xf1h02  ='Imputation field for f1h02 - Value of endowment assets at the end of the fiscal year'
f1h02   ='Value of endowment assets at the end of the fiscal year' 
xf1h03  ='Imputation field for f1h03 - Change in value of endowment net assets'
f1h03   ='Change in value of endowment net assets' 
xf1h03a ='Imputation field for f1h03a - New gifts and additions'
f1h03a  ='New gifts and additions' 
xf1h03b ='Imputation field for f1h03b - Endowment net investment return'
f1h03b  ='Endowment net investment return' 
xf1h03c ='Imputation field for f1h03c - Spending distribution for current use'
f1h03c  ='Spending distribution for current use' 
xf1h03d ='Imputation field for f1h03d - Other changes in value of endowment net assets'
f1h03d  ='Other changes in value of endowment net assets' 
xf1n01  ='Imputation field for f1n01 - Operating income (Loss) + net nonoperating revenues (expenses)'
f1n01   ='Operating income (Loss) + net nonoperating revenues (expenses)' 
xf1n02  ='Imputation field for f1n02 - Operating revenues + nonoperating revenues'
f1n02   ='Operating revenues + nonoperating revenues' 
xf1n03  ='Imputation field for f1n03 - Change in net position'
f1n03   ='Change in net position' 
xf1n04  ='Imputation field for f1n04 - Net position'
f1n04   ='Net position' 
xf1n05  ='Imputation field for f1n05 - Expendable net assets'
f1n05   ='Expendable net assets' 
xf1n06  ='Imputation field for f1n06 - Plant-related debt'
f1n06   ='Plant-related debt' 
xf1n07  ='Imputation field for f1n07 - Total expenses'
f1n07   ='Total expenses';
run;

Proc Format;
value f1mhp     
1='Yes -  pension information reported' 
2='No';
value f1mhop    
1='Yes - postemployment benefits other than pension (OPEB) reported' 
2='No';
value f1fha     
1='Yes - (report endowment assets)' 
2='No' 
-2='Not applicable';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';

Proc Freq;
Tables
xf1a01   xf1a31   xf1a04   xf1a05   xf1a06   xf1a19   xf1a07   xf1a08   xf1a09  
xf1a10   xf1a11   xf1a12   xf1a13   xf1a20   xf1a14   xf1a15   xf1a16   xf1a17   xf1a18  
xf1a214  xf1a224  xf1a234  xf1a324  xf1a274  xf1a27t4 xf1a284  xf1a334  xf1a344  xf1d01  
xf1d02   xf1d03   xf1d04   xf1d05   xf1d06   xf1b01   xf1b02   xf1b03   xf1b04   xf1b04a 
xf1b04b  xf1b05   xf1b06   xf1b26   xf1b07   xf1b08   xf1b09   xf1b10   xf1b11   xf1b12  
xf1b13   xf1b14   xf1b15   xf1b16   xf1b17   xf1b18   xf1b19   xf1b27   xf1b20   xf1b21  
xf1b22   xf1b23   xf1b24   xf1b25   xf1c011  xf1c012  xf1c021  xf1c022  xf1c031  xf1c032 
xf1c051  xf1c052  xf1c061  xf1c062  xf1c071  xf1c072  xf1c101  xf1c111  xf1c112  xf1c121 
xf1c122  xf1c131  xf1c132  xf1c141  xf1c142  xf1c191  xf1c192  xf1c193  xf1c19om xf1c19dp
xf1c19in xf1c19ot f1mhp    xf1m01   xf1m02   xf1m03   xf1m04   f1mhop   xf1m05   xf1m06  
xf1m07   xf1m08   xf1e01   xf1e02   xf1e03   xf1e04   xf1e05   xf1e06   xf1e07   xf1e08  
xf1e09   xf1e10   xf1e11   xf1e12   xf1e121  xf1e122  xf1e13   xf1e131  xf1e132  xf1e14  
xf1e141  xf1e142  xf1e15   xf1e151  xf1e152  xf1e16   xf1e161  xf1e162  xf1e17   xf1e171 
xf1e172  f1fha    xf1h01   xf1h02   xf1h03   xf1h03a  xf1h03b  xf1h03c  xf1h03d  xf1n01  
xf1n02   xf1n03   xf1n04   xf1n05   xf1n06   xf1n07    / missing;
format xf1a01  -character-xf1n07   $ximpflg.
f1mhp  f1mhp.
f1mhop  f1mhop.
f1fha  f1fha.
;

Proc Summary print n sum mean min max;
var
f1a01    f1a31    f1a04    f1a05    f1a06    f1a19    f1a07    f1a08    f1a09   
f1a10    f1a11    f1a12    f1a13    f1a20    f1a14    f1a15    f1a16    f1a17    f1a18   
f1a214   f1a224   f1a234   f1a324   f1a274   f1a27t4  f1a284   f1a334   f1a344   f1d01   
f1d02    f1d03    f1d04    f1d05    f1d06    f1b01    f1b02    f1b03    f1b04    f1b04a  
f1b04b   f1b05    f1b06    f1b26    f1b07    f1b08    f1b09    f1b10    f1b11    f1b12   
f1b13    f1b14    f1b15    f1b16    f1b17    f1b18    f1b19    f1b27    f1b20    f1b21   
f1b22    f1b23    f1b24    f1b25    f1c011   f1c012   f1c021   f1c022   f1c031   f1c032  
f1c051   f1c052   f1c061   f1c062   f1c071   f1c072   f1c101   f1c111   f1c112   f1c121  
f1c122   f1c131   f1c132   f1c141   f1c142   f1c191   f1c192   f1c193   f1c19om  f1c19dp 
f1c19in  f1c19ot  f1m01    f1m02    f1m03    f1m04    f1m05    f1m06   
f1m07    f1m08    f1e01    f1e02    f1e03    f1e04    f1e05    f1e06    f1e07    f1e08   
f1e09    f1e10    f1e11    f1e12    f1e121   f1e122   f1e13    f1e131   f1e132   f1e14   
f1e141   f1e142   f1e15    f1e151   f1e152   f1e16    f1e161   f1e162   f1e17    f1e171  
f1e172   f1h01    f1h02    f1h03    f1h03a   f1h03b   f1h03c   f1h03d   f1n01   
f1n02    f1n03    f1n04    f1n05    f1n06    f1n07    ;
run;
