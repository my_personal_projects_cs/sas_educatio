*** Created:    August 22, 2022                                ***;
*** Modify the path below to point to your data file.        ***;
***                                                          ***;
*** The specified subdirectory was not created on            ***;
*** your computer. You will need to do this.                 ***;
***                                                          ***;
*** This read program must be run against the specified      ***;
*** data file. This file is specified in the program         ***;
*** and must be saved separately.                            ***;
***                                                          ***;
*** Code was written for SAS version 8.0.                    ***;
*** Field names can be longer than 8 characters.             ***;
***                                                          ***;
*** This program does not provide frequencies or univariate  ***;
*** for all variables.                                      ***;
***                                                          ***;
*** This program does not include reserved values in its     ***;
*** calculations for missing values.  ***;
***                                                          ***;
*** There may be missing data for some institutions due      ***;
*** to the merge used to create this file.                   ***;
Data DCT;
infile 'e:\shares\ipeds\dct\gr200_21.csv' delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;

informat
unitid   6. 
xbarevct $1.
barevct  6. 
xbaexclu $1.
baexclu  6. 
xbaac150 $1.
baac150  6. 
xbanc100 $1.
banc100  6. 
xbagr100 $1.
bagr100  6. 
xbanc150 $1.
banc150  6. 
xbagr150 $1.
bagr150  6. 
xbaaexcl $1.
baaexcl  6. 
xbaac200 $1.
baac200  6. 
xbanc20a $1.
banc200a 6. 
xbastend $1.
bastend  6. 
xbanc200 $1.
banc200  6. 
xbagr200 $1.
bagr200  6. 
xl4revct $1.
l4revct  6. 
xl4exclu $1.
l4exclu  6. 
xl4ac150 $1.
l4ac150  6. 
xl4nc100 $1.
l4nc100  6. 
xl4gr100 $1.
l4gr100  6. 
xl4nc150 $1.
l4nc150  6. 
xl4gr150 $1.
l4gr150  6. 
xl4aexcl $1.
l4aexcl  6. 
xl4ac200 $1.
l4ac200  6. 
xl4nc20a $1.
l4nc200a 6. 
xl4stend $1.
l4stend  6. 
xl4nc200 $1.
l4nc200  6. 
xl4gr200 $1.
l4gr200  6.;

input
unitid   
xbarevct $
barevct  
xbaexclu $
baexclu  
xbaac150 $
baac150  
xbanc100 $
banc100  
xbagr100 $
bagr100  
xbanc150 $
banc150  
xbagr150 $
bagr150  
xbaaexcl $
baaexcl  
xbaac200 $
baac200  
xbanc20a $
banc200a 
xbastend $
bastend  
xbanc200 $
banc200  
xbagr200 $
bagr200  
xl4revct $
l4revct  
xl4exclu $
l4exclu  
xl4ac150 $
l4ac150  
xl4nc100 $
l4nc100  
xl4gr100 $
l4gr100  
xl4nc150 $
l4nc150  
xl4gr150 $
l4gr150  
xl4aexcl $
l4aexcl  
xl4ac200 $
l4ac200  
xl4nc20a $
l4nc200a 
xl4stend $
l4stend  
xl4nc200 $
l4nc200  
xl4gr200 $
l4gr200 ;

label
unitid  ='Unique identification number of the institution' 
xbarevct='Imputation field for barevct - Revised bachelor^s degree-seeking cohort, (cohort year 2013)'
barevct ='Revised bachelor^s degree-seeking cohort, (cohort year 2013)' 
xbaexclu='Imputation field for baexclu - Exclusions from bachelor^s degree-seeking cohort within 150% percent of normal time'
baexclu ='Exclusions from bachelor^s degree-seeking cohort within 150% percent of normal time' 
xbaac150='Imputation field for baac150 - Adjusted bachelor^s degree-seeking cohort within 150% of normal time'
baac150 ='Adjusted bachelor^s degree-seeking cohort within 150% of normal time' 
xbanc100='Imputation field for banc100 - Number completed a bachelor^s degree within 100% of normal time (4-years)'
banc100 ='Number completed a bachelor^s degree within 100% of normal time (4-years)' 
xbagr100='Imputation field for bagr100 - 4-year Graduation rate - bachelor^s degree within 100% of normal time'
bagr100 ='4-year Graduation rate - bachelor^s degree within 100% of normal time' 
xbanc150='Imputation field for banc150 - Number completed a bachelor^s degree within 150% of normal time (6-years)'
banc150 ='Number completed a bachelor^s degree within 150% of normal time (6-years)' 
xbagr150='Imputation field for bagr150 - 6-year Graduation rate - bachelor^s degree within 150% of normal time'
bagr150 ='6-year Graduation rate - bachelor^s degree within 150% of normal time' 
xbaaexcl='Imputation field for baaexcl - Additional exclusions from bachelor^s degree-seeking cohort'
baaexcl ='Additional exclusions from bachelor^s degree-seeking cohort' 
xbaac200='Imputation field for baac200 - Adjusted bachelor^s degree-seeking cohort within 200% of normal time'
baac200 ='Adjusted bachelor^s degree-seeking cohort within 200% of normal time' 
xbanc20a='Imputation field for banc200a - Number completed a bachelor^s degree between 150% and 200% of normal time'
banc200a='Number completed a bachelor^s degree between 150% and 200% of normal time' 
xbastend='Imputation field for bastend - Still enrolled'
bastend ='Still enrolled' 
xbanc200='Imputation field for banc200 - Number completed a bachelor^s degree within 200% of normal time (8-years)'
banc200 ='Number completed a bachelor^s degree within 200% of normal time (8-years)' 
xbagr200='Imputation field for bagr200 - 8-year Graduation rate - bachelor^s degree within 200% of normal time'
bagr200 ='8-year Graduation rate - bachelor^s degree within 200% of normal time' 
xl4revct='Imputation field for l4revct - Revised degree/certificate-seeking cohort, (cohort year 2017)'
l4revct ='Revised degree/certificate-seeking cohort, (cohort year 2017)' 
xl4exclu='Imputation field for l4exclu - Exclusions from degree/certificate-seeking cohort within 150% percent of normal time'
l4exclu ='Exclusions from degree/certificate-seeking cohort within 150% percent of normal time' 
xl4ac150='Imputation field for l4ac150 - Adjusted degree/certificate-seeking cohort within 150% of normal time'
l4ac150 ='Adjusted degree/certificate-seeking cohort within 150% of normal time' 
xl4nc100='Imputation field for l4nc100 - Number completed a degree/certificate within 100% of normal time'
l4nc100 ='Number completed a degree/certificate within 100% of normal time' 
xl4gr100='Imputation field for l4gr100 - Graduation rate - degree/certificate within 100% of normal time'
l4gr100 ='Graduation rate - degree/certificate within 100% of normal time' 
xl4nc150='Imputation field for l4nc150 - Number completed a degree/certificate  within 150% of normal time'
l4nc150 ='Number completed a degree/certificate  within 150% of normal time' 
xl4gr150='Imputation field for l4gr150 - Graduation rate - degree/certificate within 150% of normal time'
l4gr150 ='Graduation rate - degree/certificate within 150% of normal time' 
xl4aexcl='Imputation field for l4aexcl - Additional exclusions from degree/certificate-seeking cohort'
l4aexcl ='Additional exclusions from degree/certificate-seeking cohort' 
xl4ac200='Imputation field for l4ac200 - Adjusted degree/certificate-seeking cohort within 200% of normal time'
l4ac200 ='Adjusted degree/certificate-seeking cohort within 200% of normal time' 
xl4nc20a='Imputation field for l4nc200a - Number completed a  degree/certificate between 150% and 200% of normal time'
l4nc200a='Number completed a  degree/certificate between 150% and 200% of normal time' 
xl4stend='Imputation field for l4stend - Still enrolled'
l4stend ='Still enrolled' 
xl4nc200='Imputation field for l4nc200 - Number completed a degree/certificate within 200% of normal time'
l4nc200 ='Number completed a degree/certificate within 200% of normal time' 
xl4gr200='Imputation field for l4gr200 - Graduation rate - degree/certificate within 200% of normal time'
l4gr200 ='Graduation rate - degree/certificate within 200% of normal time';
run;

Proc Format;
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program not applicable' 
Z='Implied zero';

Proc Freq;
Tables
xbarevct xbaexclu xbaac150 xbanc100 xbagr100 xbanc150 xbagr150 xbaaexcl xbaac200
xbanc20a xbastend xbanc200 xbagr200 xl4revct xl4exclu xl4ac150 xl4nc100 xl4gr100 xl4nc150
xl4gr150 xl4aexcl xl4ac200 xl4nc20a xl4stend xl4nc200 xl4gr200  / missing;
format xbarevct-character-xl4gr200 $ximpflg.;

Proc Summary print n sum mean min max;
var
barevct  baexclu  baac150  banc100  bagr100  banc150  bagr150  baaexcl  baac200 
banc200a bastend  banc200  bagr200  l4revct  l4exclu  l4ac150  l4nc100  l4gr100  l4nc150 
l4gr150  l4aexcl  l4ac200  l4nc200a l4stend  l4nc200  l4gr200  ;
run;
