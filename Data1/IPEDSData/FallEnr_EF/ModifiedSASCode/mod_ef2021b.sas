%let path=/Data1/IPEDSData/DataFiles/FallEnr-EF;
%let file=ef2021b;
%let libraryName=IPEDEF;
libname &libraryName "&path/SASData";

Proc Format library = &libraryName;
value efbage    
1='All age categories total' 
2='Age under 25 total' 
3='Age under 18' 
4='Age 18-19' 
5='Age 20-21' 
6='Age 22-24' 
7='Age 25 and over total' 
8='Age 25-29' 
9='Age 30-34' 
10='Age 35-39' 
11='Age 40-49' 
12='Age 50-64' 
13='Age 65 and over' 
14='Age unknown';
value line      
1='Age 18 and under' 
2='Age 18-19' 
3='Age 20-21' 
4='Age 22-24' 
5='Age 25-29' 
6='Age 30-34' 
7='Age 35-39' 
8='Age 40-49' 
9='Age 50-64' 
10='Age 65 and over' 
11='Age unknown' 
112='Undergraduate, total' 
312='Graduate total' 
412='Total, all students' 
999='Generated record not on survey form';
value lstudy    
1='All Students total' 
2='Undergraduate' 
5='Graduate';
value $ximpflg  
A='Not applicable' 
B='Institution left item blank' 
C='Analyst corrected reported value' 
D='Do not know' 
G='Data generated from other data values' 
H='Value not derived - data not usable' 
J='Logical imputation' 
K='Ratio adjustment' 
L='Imputed using the Group Median procedure' 
N='Imputed using Nearest Neighbor procedure' 
P='Imputed using Carry Forward procedure' 
R='Reported' 
Y='Specific professional practice program n' 
Z='Implied zero';



options fmtsearch=(&libraryName);

data &libraryName..&file;
  infile "&path/&file..csv" delimiter=',' DSD MISSOVER firstobs=2 lrecl=32736;
  informat unitid 6. efbage 5. line 5. lstudy 2.  xefage01 $1. efage01 6. 
    xefage02  $1. efage02 6. xefage03  $1. efage03 6. xefage04   $1. efage04 6. 
    xefage05 $1. efage05 6. xefage06 $1. efage06 6. xefage07   $1. efage07 6. 
    xefage08 $1. efage08 6. xefage09 $1. efage09 6.;
  input unitid efbage line lstudy xefage01 $
efage01 xefage02 $
efage02 xefage03 $
efage03 xefage04 $
efage04 xefage05 $
efage05 xefage06 $
efage06 xefage07 $
efage07 xefage08 $
efage08 xefage09 $
efage09 ;
  label unitid  ='Unique identification number of the institution' 
efbage  ='Age category' line    ='Original line number on survey form' 
lstudy  ='Level of student' xefage01='Imputation field for efage01 - Full time men'
efage01 ='Full time men' xefage02='Imputation field for efage02 - Full time women'
efage02 ='Full time women' xefage03='Imputation field for efage03 - Part time men'
efage03 ='Part time men' xefage04='Imputation field for efage04 - Part time women'
efage04 ='Part time women' xefage05='Imputation field for efage05 - Full time total'
efage05 ='Full time total' xefage06='Imputation field for efage06 - Part time total'
efage06 ='Part time total' xefage07='Imputation field for efage07 - Total men'
efage07 ='Total men' xefage08='Imputation field for efage08 - Total women'
efage08 ='Total women' xefage09='Imputation field for efage09 - Grand total'
efage09 ='Grand total';
  format xefage01-character-xefage09 $ximpflg.
efbage  efbage.
line  line.
lstudy  lstudy.
;

run;


Proc Freq;
Tables
efbage   line     lstudy   xefage01 xefage02 xefage03 xefage04 xefage05 xefage06
xefage07 xefage08 xefage09  / missing;
format xefage01-character-xefage09 $ximpflg.
efbage  efbage.
line  line.
lstudy  lstudy.
;

Proc Summary print n sum mean min max;
var
efage01  efage02  efage03  efage04  efage05  efage06 
efage07  efage08  efage09  ;
run;