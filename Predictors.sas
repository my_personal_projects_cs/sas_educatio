%let path=/Data1/IPEDSData/DataFiles/GraduationRates-GR;
%let libraryName=IPEDGR;
libname &libraryName "&path/SASData";
%let path=/Data1/IPEDSData/DataFiles/Salaries-SAL;
%let libraryName=ipedsal;
libname &libraryName "&path/SASData";
%let path=/Data1/IPEDSData/DataFiles/InstCharacteristics-IC;
%let libraryName=ipedicm;
libname &libraryName "&path/SASData";
%let path=/Data1/IPEDSData/DataFiles/FinancialAid-SFA;
%let libraryName=ipedsfa;
libname &libraryName "&path/SASData";

/*** Average Salary across instructional staff ***/
data salarypredictor;
    set ipedsal.sal2021_is;
    keep unitid avgsal avgsalW avgsalM;
    where arank eq 7;
    avgsal = saoutlt/sainstt;
    avgsalW = saoutlw/sainstw;
    avgsalM = saoutlm/sainstw;
run;

/**Creating in and out of state cost of attendence predictors**/
data costattenpredictor;
	set ipedicm.ic2021_ay;
	keep unitid cost_in cost_out;
	avg_cost = SUM(chg5ay2, chg6ay2, chg7ay2, chg8ay2)/2;
	cost_in = chg2ay2 + chg4ay2 + avg_cost;
	cost_out = chg3ay2 + chg4ay2 + avg_cost;
	where chg2ay2 ne . and chg3ay2 ne . and chg4ay2 ne .;
run;
/**Average percent of cost of attendence covered by financial aid**/
data pctcovfinaidpredictor;
	merge ipedsfa.sfa2021 costattenpredictor;
	by unitid;
	if uagrnta ne . and cost_in ne . and cost_out ne .;
	keep unitid cost_in cost_out pctcost;
	if scfa13p = . then do;
		per_stu = cost_out;
		end;
	else do;
		per_stu = ((cost_out) * scfa13p/100) + ((cost_in) * (1-(scfa13p/100)));
		end;
	pctcost = 100 * (uagrnta / per_stu);
run;

proc format;
value obereg  
0='U.S. Service schools' 
1,2='New England and Mid East' 
3,4='Great Lakes and Plains' 
5='Southeast (AL, AR, FL, GA, KY, LA, MS, NC, SC, TN, VA, WV)' 
6='Southwest (AZ, NM, OK, TX)' 
7='Rocky Mountains (CO, ID, MT, UT, WY)' 
8='Far West (AK, CA, HI, NV, OR, WA)' 
9='Other U.S. jurisdictions (AS, FM, GU, MH, MP, PR, PW, VI)'
;
run;

data merges;
merge  ipedicm.ic2021_ay(in=b)  ipedsfa.sfa2021(in=d) ipedicm.hd2021(in=e) ipedicm.ic2021(in=f);
by unitid;
if b and d and e and f;
drop x:;
run;

data preds;
 set merges;
 where not missing(tuition2) and uagrnta ne . and upgrnta ne . and ufloana ne .;
 AvgGrant = uagrnta;
 AvgPell = upgrnta;
 AvgFed = ufloana;
 obereg2 = put(obereg,obereg.);
format instsize instsize.;  
 keep unitid AvgGrant AvgPell AvgFed obereg obereg2;
run;

/*  use merges to merge */


data f2021_f01;
  set ipedf.f2021_f1a(keep=unitid f1c011--f1c072 f1c111--f1c122 f1c141--f1c192);
  drop x:;
run;

data f2021_f02;
  set ipedf.f2021_f2(keep=unitid f2e011--f2e072 f2e091--f2e092 f2e121--f2e132);
  drop x:;
run;

data f2021_f03;
  set ipedf.f2021_f3(keep=unitid f3e011--f3e042 f3e101--f3e072);
  drop x:;
run;

proc sql;
  create table f2021 as
  select *
  from f2021_f01
  union
  select *
  from f2021_f02
  union
  select *
  from f2021_f03
  ;
quit;

data costs;
	set ipedic.ic2021_ay;
	cost_in = sum(chg2ay2, chg4ay2, mean(sum(chg5ay2, chg6ay2), sum(chg7ay2, chg8ay2)));
	cost_out = sum(chg3ay2, chg4ay2, mean(sum(chg5ay2, chg6ay2), sum(chg7ay2, chg8ay2)));
	label cost_in = 'Cost of attendance for in-state students'
		cost_out = 'Cost of attendance for out-of-state students';
	keep unitid cost_in cost_out;
run;

data salout;
	set ipedsal.sal2021_is;
	where arank = 7;
	avgsaltot = saoutlt/sainstt;
	if sainstm ne 0 then avgsalmen = saoutlm/sainstm;
	if sainstw ne 0 then avgsalwom = saoutlw/sainstw;
	staffprop = sainstm/sainstt;
	if saoutlt ne 0 then salaryprop = saoutlm/saoutlt;
	label avgsaltot = 'Average salary for all instructional staff'
		avgsalmen = 'Average salary for male instructional staff'
		avgsalwom = 'Average salary for female instructional staff'
		staffprop = 'Proportion of instructional staff who are male'
		salaryprop = 'Proportion of salary outlays for male instructional staff';
	keep unitid avgsaltot avgsalmen avgsalwom staffprop salaryprop;
run;

proc sql;
	create table bach_count as
	select unitid, pbachl
	from ipedc.c2021dep
	where cipcode eq '99' /*we want counts of programs across ALL departments*/
	;
	create table ef as
	select unitid, efage01, efage02, efage03, efage04, efage05, efage06, efage07, efage08, efage09
	from ipedef.ef2021b
	where efbage eq 1 and lstudy eq 2; /*all ages, undergraduates only*/
	;
quit;



